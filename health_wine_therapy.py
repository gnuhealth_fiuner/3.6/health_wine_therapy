from trytond.model import ModelSQL, ModelView, fields
from trytond.pyson import Eval


class Wine(ModelSQL, ModelView):
    'Wine'
    __name__ = 'gnuhealth.wine'

    product = fields.Many2One('product.product', 'Product',
                    required=True)
    harvest = fields.Date('Haverst date', required=True)

    def get_rec_name(self, name):
        if self.product and self.harvest:
            return self.product.rec_name + '*'+ \
                self.harvest.strftime('%d/%m/%Y')
        return ''

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('product',) + tuple(clause[1:]),
            ]


class WineCup(ModelSQL, ModelView):
    'Wine Cup'
    __name__ = 'gnuhealth.wine.cup'

    wine = fields.Many2One('gnuhealth.wine', 'Wine', required=True)
    daily_cups = fields.Integer('Daily cups', required=True,
            domain=[('daily_cups','>',0)], help='Daily cups for therapy')
    start_date = fields.Date('Start date', required=True)
    end_date = fields.Date('End date', required=True,
            domain=[('end_date', '>', Eval('start_date'))])
    therapy = fields.Many2One('gnuhealth.wine.therapy',
            'Therapy', required=True)


class WineTherapy(ModelSQL, ModelView):
    'Wine Therapy'
    __name__ = 'gnuhealth.wine.therapy'

    patient = fields.Many2One('gnuhealth.patient', 'Patient',
            required=True)
    cups = fields.One2Many('gnuhealth.wine.cup', 'therapy', 'Cups')
    wine_allowed = fields.Many2Many('gnuhealth.wine.therapy-wine',
            'therapy', 'wine', 'Wines allowed')
    state = fields.Selection([
        (None, ''),
        ('in_progress', 'In progress'),
        ('done', 'Done'),
        ('cancel', 'Cancel')
        ], 'State', sort=False, readonly=True)
    total_cups = fields.Function(
        fields.Integer('Total cups',
            help='Total cups of wine for this therapy'),
        'on_change_with_total_cups')

    @fields.depends('cups')
    def on_change_with_total_cups(self, name=None):
        if self.cups:
            return sum([cup.daily_cups for cup in self.cups if cup.daily_cups])
        return 0

    @staticmethod
    def default_state():
        return 'in_progress'


class WineTherapyWine(ModelSQL):
    'Wine Therapy - Wine'
    __name__ = 'gnuhealth.wine.therapy-wine'

    wine = fields.Many2One('gnuhealth.wine', 'Wine', required=True,
                    ondelete='CASCADE')
    therapy = fields.Many2One('gnuhealth.wine.therapy', 'Therapy', required=True,
                    ondelete='CASCADE')
