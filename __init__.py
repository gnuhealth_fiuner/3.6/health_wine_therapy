from trytond.pool import Pool

from . import health_wine_therapy

def register():
    Pool.register(
        health_wine_therapy.Wine,
        health_wine_therapy.WineCup,
        health_wine_therapy.WineTherapy,
        health_wine_therapy.WineTherapyWine,
        module='health_wine_therapy', type_='model')
